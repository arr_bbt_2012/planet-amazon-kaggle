# GENERAL
PROJECT = "planet-amazon-kaggle"
MLFLOW_BUCKET = f"gs://ml-rnd/{PROJECT}/mlflow"
CACHE_DIR = "cache"
DATA_DIR = 'data'