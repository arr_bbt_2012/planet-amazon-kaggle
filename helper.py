import logging
import random
import re
import subprocess
import sys
from math import floor

from torch.utils.data import Sampler

import config as cfg

loggers = {}


def get_logger():
    global loggers
    if not __name__ in loggers:
        logger = logging.getLogger(__name__)
        logger.setLevel(logging.INFO)
        formatter = logging.Formatter('%(asctime)s %(module)-8s %(levelname)-8s %(message)s', '%Y-%m-%d %H:%M:%S')
        handler = logging.StreamHandler(stream=sys.stdout)
        handler.setFormatter(formatter)
        logger.addHandler(handler)
        loggers[__name__] = logger
    return loggers[__name__]


logger = get_logger()


def sync_mlflow_data():
    logger.info("sync mlflow data")
    try:
        subprocess.check_output(["gsutil", "-m", "-q", "rsync", "-r", "mlruns", 
                                 "{}/mlruns".format(cfg.MLFLOW_BUCKET)],
                                stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError as e:
        print('command {} failed'.format(' '.join(e.cmd)))
        print('mlflow run exception {}'.format(e.output.decode()))
        exit(1)


def check_repo_changes():
    logger.info("checking if there are any changes since last commit")
    try:
        output = subprocess.check_output(["git", "diff", "HEAD"], stderr=subprocess.STDOUT)
        if not output.decode():
            pass
        else:
            print("there are some changes in repo, please commit them")
            exit(1)
    except subprocess.CalledProcessError as e:
        print('command {} failed'.format(' '.join(e.cmd)))
        print('mlflow run exception {}'.format(e.output.decode()))
        exit(1)


def get_git_info():
    logger.info("get git branch name and origin url")
    git_branch_name = ''
    try:
        git_branch_name = subprocess.check_output(["git", "branch"], stderr=subprocess.STDOUT)
        git_branch_name = re.search(r'\* (.*)\n', git_branch_name.decode()).group(1)
    except subprocess.CalledProcessError as e:
        print('command {} failed'.format(' '.join(e.cmd)))
        print('mlflow run exception {}'.format(e.output.decode()))
        exit(1)
    git_commit_hash = ''
    try:
        git_commit_hash = subprocess.check_output(["git", "rev-parse", "HEAD"], stderr=subprocess.STDOUT)
        git_commit_hash = git_commit_hash.decode()
    except subprocess.CalledProcessError as e:
        print('command {} failed'.format(' '.join(e.cmd)))
        print('mlflow run exception {}'.format(e.output.decode()))
        exit(1)
    git_origin_url = ''
    try:
        git_origin_url = subprocess.check_output(["git", "config", "--get", "remote.origin.url"],
                                                 stderr=subprocess.STDOUT)
        git_origin_url = re.search(r'git@(.*)\.git', git_origin_url.decode()).group(1)
        git_origin_url = "https://" + git_origin_url.replace(":", "/") + "/src/" + git_commit_hash
    except subprocess.CalledProcessError as e:
        print('command {} failed'.format(' '.join(e.cmd)))
        print('mlflow run exception {}'.format(e.output.decode()))
        exit(1)
    return git_branch_name, git_origin_url


class SubsetSampler(Sampler):
    """Samples elements from a given list of indices.

    Arguments:
        indices (list): a list of indices
    """

    def __init__(self, indices):
        self.num_samples = len(indices)
        self.indices = indices

    def __iter__(self):
        return iter(self.indices)

    def __len__(self):
        return self.num_samples


def train_valid_split(dataset, test_size=0.25, shuffle=False, random_seed=0):
    """ Return a list of splitted indices from a DataSet.
    Indices can be used with DataLoader to build a train and validation set.

    Arguments:
        A Dataset
        A test_size, as a float between 0 and 1 (percentage split) or as an int (fixed number split)
        Shuffling True or False
        Random seed
    """
    length = dataset.__len__()
    indices = list(range(1, length))

    if shuffle == True:
        random.seed(random_seed)
        random.shuffle(indices)

    if type(test_size) is float:
        split = floor(test_size * length)
    elif type(test_size) is int:
        split = test_size
    else:
        raise ValueError('%s should be an int or a float' % str)
    return indices[split:], indices[:split]


def epoch_time(start_time, end_time):
    elapsed_time = end_time - start_time
    elapsed_mins = int(elapsed_time / 60)
    elapsed_secs = int(elapsed_time - (elapsed_mins * 60))
    return elapsed_mins, elapsed_secs