import torch

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


def metric(y_true, y_pred, beta=2, threshold=0.2, eps=1e-9):
    beta2 = beta**2

    y_pred = torch.ge(y_pred.float(), threshold).float()
    y_true = y_true.float()

    true_positive = (y_pred * y_true).sum(dim=1)
    precision = true_positive.div(y_pred.sum(dim=1).add(eps))
    recall = true_positive.div(y_true.sum(dim=1).add(eps))

    return torch.mean(
        (precision*recall).
        div(precision.mul(beta2) + recall + eps).
        mul(1 + beta2))


def train(model, iterator, optimizer, criterion):
    epoch_loss = 0
    epoch_acc = 0

    model.train()

    for batch in iterator:
        inputs, target = batch
        inputs = inputs.to(device)
        target = target.to(device)

        optimizer.zero_grad()

        predictions = model(inputs)
        loss = criterion(predictions.squeeze(), target.float())

        model.eval()
        acc = metric(target, predictions.squeeze())

        loss.backward()

        optimizer.step()

        epoch_loss += loss.item()
        epoch_acc += acc.item()

    return epoch_loss / len(iterator), epoch_acc / len(iterator)


def evaluate(model, iterator, criterion):
    epoch_loss = 0
    epoch_acc = 0

    model.eval()

    with torch.no_grad():
        for batch in iterator:
            inputs, target = batch
            inputs = inputs.to(device)
            target = target.to(device)

            predictions = model(inputs)
            loss = criterion(predictions.squeeze(), target.float())

            acc = metric(target, predictions.squeeze())

            epoch_loss += loss.item()
            epoch_acc += acc.item()

    return epoch_loss / len(iterator), epoch_acc / len(iterator)