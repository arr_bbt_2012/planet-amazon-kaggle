import os
import time

import mlflow
import torch
import torch.optim as optim
from PIL import Image
from torch import nn
from torch.utils.data import DataLoader
from torch.utils.data import SubsetRandomSampler
from torchvision import transforms
import config as cfg
from config import DATA_DIR

from dataset import KaggleAmazonDataset
from helper import sync_mlflow_data, check_repo_changes, get_git_info, SubsetSampler, train_valid_split, epoch_time
from model import Net
from trainer import train, evaluate


def prepare_data():
    # ! cp ~/Downloads/sample_submission_v2.csv.zip {DATA_DIR} && unzip sample_submission_v2.csv.zip -d {DATA_DIR}
    # ! cp ~/Downloads/train_v2.csv {DATA_DIR}
    # ! cp ~/Downloads/train-jpg.tar.7z {DATA_DIR}
    # ! 7za -bd -y -so x {DATA_DIR}/train-jpg.tar.7z | tar xf - -C {DATA_DIR.as_posix()}
    # ! cp ~/Downloads/test-jpg.tar.7z {DATA_DIR} && cp ~/Downloads/test-jpg-additional.tar.7z {DATA_DIR}
    # ! 7za -bd -y -so x {DATA_DIR}/test-jpg.tar.7z | tar xf - -C {DATA_DIR}
    # ! 7za -bd -y -so x {DATA_DIR}/test-jpg-additional.tar.7z | tar xf - -C {DATA_DIR}
    # ! cp ~/Downloads/sample_submission.csv {DATA_DIR}
    if not os.path.exists(DATA_DIR):
        os.mkdir(DATA_DIR)


def get_dataloaders():
    IMG_PATH = f'{DATA_DIR}/train-jpg/'
    IMG_EXT = '.jpg'
    TRAIN_DATA = f'{DATA_DIR}/train_v2.csv'

    transformations = transforms.Compose([transforms.Resize(32), transforms.ToTensor()])

    X_train = KaggleAmazonDataset(TRAIN_DATA, IMG_PATH, IMG_EXT,
                                  transformations)
    X_val = KaggleAmazonDataset(TRAIN_DATA, IMG_PATH, IMG_EXT,
                                transformations)

    # train_idx, valid_idx = train_test_split(X_train, test_size=0.2)
    train_idx, valid_idx = train_valid_split(X_train, test_size=0.2)

    train_sampler = SubsetRandomSampler(train_idx)
    valid_sampler = SubsetSampler(valid_idx)

    train_loader = DataLoader(X_train,
                              batch_size=256,
                              sampler=train_sampler,
                              num_workers=1)

    valid_loader = DataLoader(X_val,
                              batch_size=256,
                              sampler=valid_sampler,
                              num_workers=1)
    return train_loader, valid_loader


def train_and_evaluate():
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    model = Net().to(device)
    optimizer = optim.SGD(model.parameters(), lr=0.01, momentum=0.5)
    criterion = nn.BCELoss().to(device)
    train_loader, valid_loader = get_dataloaders()

    N_EPOCHS = 10

    best_valid_loss = float('inf')
    best_valid_acc = float('inf')

    for epoch in range(N_EPOCHS):
        start_time = time.time()

        train_loss, train_acc = train(model, train_loader, optimizer, criterion)
        valid_loss, valid_acc = evaluate(model, valid_loader, criterion)

        end_time = time.time()

        epoch_mins, epoch_secs = epoch_time(start_time, end_time)

        if valid_loss < best_valid_loss:
            best_valid_loss = valid_loss
            torch.save(model.state_dict(), 'model.pt')
            best_valid_acc = valid_acc

        print(f'Epoch: {epoch + 1:02} | Epoch Time: {epoch_mins}m {epoch_secs}s')
        print(f'\tTrain Loss: {train_loss:.3f} | Train Acc: {train_acc * 100:.2f}%')
        print(f'\t Val. Loss: {valid_loss:.3f} |  Val. Acc: {valid_acc * 100:.2f}%')
    return {'valid_acc': best_valid_acc}


def get_predictions():
    TEST_IMG_PATH = f'{DATA_DIR}/test-jpg/'
    img = Image.open(TEST_IMG_PATH + 'test_0.jpg')
    img = img.convert('RGB')
    model = Net().cuda()
    model.load_state_dict(torch.load('model.pt'))
    model.eval()
    transformations = transforms.Compose([transforms.Scale(32), transforms.ToTensor()])
    img_data = transformations(img).cuda().unsqueeze(0)
    pred = model(img_data)
    return pred


if __name__ == "__main__":
    check_repo_changes()
    git_branch_name, git_origin_url = get_git_info()

    mlflow.set_experiment(cfg.PROJECT)
    with mlflow.start_run(run_name=git_branch_name):
        tags = {
            'git_origin_url': git_origin_url
        }
        mlflow.set_tags(tags)

        prepare_data()
        metrics = train_and_evaluate()
        mlflow.log_metrics(metrics)

        sync_mlflow_data()
